package model;

//contoh kelas model atau plain old java object
public class SamsungGalaxyNote {

	//atribut atau properti dari sebuah class
	private int cpu; // field/ atribut / property
	public int batre;
	
	String specialTrait; //bu default protected
	
	public static String PI = "3.14"; //konstanta
	

	//constructor
	public SamsungGalaxyNote() {
		this.cpu = 4;
		this.batre = 100; 
		this.specialTrait = "bisa meledak";
	}
	
	//constructor2
	public SamsungGalaxyNote(int paramCpu, int paramBatre, String paramSpecialTrait) {
		this.cpu = paramCpu;
		this.batre = paramBatre;
		this.specialTrait = paramSpecialTrait;
	}
	
	//constructor 3
	public SamsungGalaxyNote(int cpu,int batre) {
		this.cpu = cpu;
		this.batre = batre;
		this.specialTrait = "bisa meledak";
	}

	//getter method
	public int getCpu() {
		//method body
		return cpu;
//		return 0;
//		return "Apa aja ga bole"; //type mismatch jika tidak sesuai return data type yg dijanjikan
	}

	//setter method
	public void setCpu(int cpu) {
		this.cpu = cpu;
	}
	
	
	
}
