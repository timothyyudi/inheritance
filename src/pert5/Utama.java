package pert5;

import model.SamsungGalaxyNote;

public class Utama {

	public static void main(String[] args) {
		
		// buat objek galaxy note baru dengan memanggil constructor, 
		// setelah dibuat objek akan masuk ram
		SamsungGalaxyNote hpAndre = new SamsungGalaxyNote(); //objek 1, local variabel
		SamsungGalaxyNote hpBudi = new SamsungGalaxyNote(); //objek 2, local variabel
		SamsungGalaxyNote hpCecil = new SamsungGalaxyNote(4,200); //objek 3, local variabel
		
//		hpAndre.cpu = 1; //manipulasi objek dengan atribut private, butuh setter getter
		hpAndre.setCpu(1); //manipulasi objek dengan atribut private, butuh setter getter
		hpBudi.batre = 50; //manipulasi objek dengan atribut public
//		hpCecil.specialTrait = "bisa meledak 2"; // manipulasi objek dengan atribut protected (default)
		
		System.out.println("cetak instance: "+hpAndre);
		System.out.println("cetak atribut Andre: "+hpAndre.getCpu());
		hpAndre.setCpu(100);
		System.out.println("cetak atribut Andre: "+hpAndre.getCpu());
		System.out.println("cetak atribut Cecil: "+hpCecil.getCpu());
		
		System.out.println("cetak var static hpAndre: "+hpAndre.PI);
		System.out.println("cetak var static hpBudi: "+hpBudi.PI);
		System.out.println("cetak var static hpCecil: "+hpCecil.PI);
		SamsungGalaxyNote.PI = "7777";
		System.out.println("cetak var static hpAndre: "+hpAndre.PI);
		System.out.println("cetak var static hpBudi: "+hpBudi.PI);
		System.out.println("cetak var static hpCecil: "+hpCecil.PI);
	}

}
